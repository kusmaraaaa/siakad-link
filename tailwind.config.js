/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./dist/**/*.{html,js}"],
  theme: {
    extend: {
      fontSize: {
        h1: ["64px", { fontWeight: "600" }],
        h2: ["48px", { fontWeight: "600" }],
        h3: ["36px", { fontWeight: "600" }],
        h4: ["24px", { fontWeight: "600" }],
        p1: ["20px", { fontWeight: "500" }],
        p2: ["16px", { fontWeight: "400", lineHeight: "2.5rem" }],
        p2bold: ["16px", { fontWeight: "600", lineHeight: "2.5rem" }],
        p3: ["14px", { fontWeight: "400", lineHeight: "2.5rem" }],

        h1Res: ["36px", { fontWeight: "600" }],
        h2Res: ["28px", { fontWeight: "600" }],
        h3Res: ["20px", { fontWeight: "600" }],
        h4Res: ["16px", { fontWeight: "600" }],
        p1Res: ["14px", { fontWeight: "500" }],
        p2Res: ["12px", { fontWeight: "400", lineHeight: "1.5rem" }],
        p2boldRes: ["12px", { fontWeight: "600", lineHeight: "2.5rem" }],
        p3Res: ["10px", { fontWeight: "400", lineHeight: "2.5rem" }],
      },
      colors: {
        blackCustom: "#1D1D1D",
        bg: "#FCFCFC",
        primary: "#C30000",
        secondary: "#364051",
        text1: "#1D1D1D",
        text2: "rgba(29,29,29,0.8)",
        text3: "rgba(29,29,29,0.5)",
      },
      fontFamily: {
        poppins: "Poppins",
      },
      backgroundImage: {
        contactus: "url('./img/contact-us-img.png')",
      },
    },
  },
  plugins: [],
};
